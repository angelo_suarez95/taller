/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     31/10/2020 03:42:47 p.m.                     */
/*==============================================================*/


drop table AUDITORIAS;

drop index CLASIFICACIONES_NOMBRE;

drop table CLASIFICACIONES;

drop index CLIENTES_DOCUMENTO;

drop table CLIENTES;

drop index EMPLEADO_DOCUMENTO;

drop table EMPLEADOS;

drop table HORARIOS;

drop table PRESUPUESTOS;

drop table PRESUPUESTOS_DETALLES;

drop index PRODUCTOS_CODIGO;

drop table SERVICIOS_PRODUCTOS;

drop index TALLERES_NOMBRE;

drop table TALLERES;

drop index UNIDADES_SIGLA;

drop index UNIDADES_NOMBRE;

drop table UNIDADES;

drop table USUARIOS;

drop index VEHICULOS_PLACA;

drop table VEHICULO;

drop table VENTAS;

drop table VENTAS_DETALLES;

drop domain D_CHAR1;

drop domain D_INT2;

drop domain D_INT4;

drop domain D_NUMERIC12;

drop domain D_NUMERIC4;

drop domain D_NUMERIC8_2;

drop domain D_TIME;

drop domain D_TIMESTAMP;

drop domain D_VARCHAR15;

drop domain D_VARCHAR20;

drop domain D_VARCHAR200;

drop domain D_VARCHAR60;

/*==============================================================*/
/* Domain: D_CHAR1                                              */
/*==============================================================*/
create domain D_CHAR1 as CHAR(1);

/*==============================================================*/
/* Domain: D_INT2                                               */
/*==============================================================*/
create domain D_INT2 as INT2;

/*==============================================================*/
/* Domain: D_INT4                                               */
/*==============================================================*/
create domain D_INT4 as INT4;

/*==============================================================*/
/* Domain: D_NUMERIC12                                          */
/*==============================================================*/
create domain D_NUMERIC12 as NUMERIC(12);

/*==============================================================*/
/* Domain: D_NUMERIC4                                           */
/*==============================================================*/
create domain D_NUMERIC4 as numeric(4);

/*==============================================================*/
/* Domain: D_NUMERIC8_2                                         */
/*==============================================================*/
create domain D_NUMERIC8_2 as NUMERIC(10,2);

/*==============================================================*/
/* Domain: D_TIME                                               */
/*==============================================================*/
create domain D_TIME as TIME;

/*==============================================================*/
/* Domain: D_TIMESTAMP                                          */
/*==============================================================*/
create domain D_TIMESTAMP as TIMESTAMP;

/*==============================================================*/
/* Domain: D_VARCHAR15                                          */
/*==============================================================*/
create domain D_VARCHAR15 as varchar(15);

/*==============================================================*/
/* Domain: D_VARCHAR20                                          */
/*==============================================================*/
create domain D_VARCHAR20 as VARCHAR(20);

/*==============================================================*/
/* Domain: D_VARCHAR200                                         */
/*==============================================================*/
create domain D_VARCHAR200 as varchar(200);

/*==============================================================*/
/* Domain: D_VARCHAR60                                          */
/*==============================================================*/
create domain D_VARCHAR60 as varchar(60);

/*==============================================================*/
/* Table: AUDITORIAS                                            */
/*==============================================================*/
create table AUDITORIAS (
   ID_AUDITORIA         SERIAL               not null,
   ID_USUARIO           D_VARCHAR20          not null,
   FECHA                D_TIMESTAMP          not null,
   TABLA                D_VARCHAR20          not null,
   ACCION               D_CHAR1              not null,
   DESCRICPCION         D_VARCHAR200         null,
   constraint PK_AUDITORIAS primary key (ID_AUDITORIA)
);

/*==============================================================*/
/* Table: CLASIFICACIONES                                       */
/*==============================================================*/
create table CLASIFICACIONES (
   ID_CLASIFICACION     SERIAL               not null,
   NOMBRE               D_VARCHAR60          not null,
   constraint PK_CLASIFICACIONES primary key (ID_CLASIFICACION)
);

/*==============================================================*/
/* Index: CLASIFICACIONES_NOMBRE                                */
/*==============================================================*/
create unique index CLASIFICACIONES_NOMBRE on CLASIFICACIONES (
NOMBRE
);

/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES (
   ID_CLIENTE           SERIAL               not null,
   NOMBRE               D_VARCHAR60          not null,
   DOCUMENTO            D_VARCHAR15          not null,
   DIGITO               D_CHAR1              null,
   DIRECCION            D_VARCHAR200         not null,
   LOCALIDAD            D_VARCHAR60          not null,
   TELEFONO_PRINCIPAL   D_VARCHAR20          not null,
   TELEFONO_SECUNDARIO  D_VARCHAR20          null,
   EMAIL                D_VARCHAR60          null,
   ESTADO               D_CHAR1              not null,
   constraint PK_CLIENTES primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTES_DOCUMENTO                                    */
/*==============================================================*/
create unique index CLIENTES_DOCUMENTO on CLIENTES (
DOCUMENTO
);

/*==============================================================*/
/* Table: EMPLEADOS                                             */
/*==============================================================*/
create table EMPLEADOS (
   ID_EMPLEADO          SERIAL               not null,
   NOMBRE               D_VARCHAR60          not null,
   DOCUMENTO            D_VARCHAR15          not null,
   DIRECCION            D_VARCHAR200         not null,
   LOCALIDAD            D_VARCHAR60          not null,
   TELEFONO_PRINCIPAL   D_VARCHAR20          not null,
   TELEFONO_SECUNDARIO  D_VARCHAR20          null,
   EMAIL                D_VARCHAR60          null,
   CARGO                D_VARCHAR60          not null,
   ESTADO               D_CHAR1              not null,
   constraint PK_EMPLEADOS primary key (ID_EMPLEADO)
);

/*==============================================================*/
/* Index: EMPLEADO_DOCUMENTO                                    */
/*==============================================================*/
create unique index EMPLEADO_DOCUMENTO on EMPLEADOS (
DOCUMENTO
);

/*==============================================================*/
/* Table: HORARIOS                                              */
/*==============================================================*/
create table HORARIOS (
   ID_HORARIO           SERIAL               not null,
   ID_TALLER            INT4                 not null,
   DIA                  D_CHAR1              not null,
   DESDE                D_TIME               not null,
   HASTA                D_TIME               not null,
   constraint PK_HORARIOS primary key (ID_HORARIO)
);

/*==============================================================*/
/* Table: PRESUPUESTOS                                          */
/*==============================================================*/
create table PRESUPUESTOS (
   ID_PRESUPUESTO       D_INT4               not null,
   ID_TALLER            INT4                 not null,
   ID_CLIENTE           INT4                 not null,
   ID_VEHICULO          INT4                 not null,
   FECHA                D_TIMESTAMP          not null,
   FECHA_VALIDEZ        D_TIMESTAMP          null,
   PRESUPUESTADO_POR    INT4                 not null,
   TOTAL                D_NUMERIC12          not null,
   CONDICION            D_CHAR1              not null,
   OBSERVACION          D_VARCHAR200         null,
   ESTADO               D_CHAR1              not null,
   constraint PK_PRESUPUESTOS primary key (ID_PRESUPUESTO)
);

/*==============================================================*/
/* Table: PRESUPUESTOS_DETALLES                                 */
/*==============================================================*/
create table PRESUPUESTOS_DETALLES (
   ID_PRESUPUESTO       INT4                 not null,
   ITEM                 D_INT2               not null,
   ID_SERVICIO_PRODUCTO INT4                 not null,
   DESCRIPCION          D_VARCHAR200         null,
   CANTIDAD             D_NUMERIC8_2         not null,
   PRECIO               D_NUMERIC12          not null,
   SUBTOTAL             D_NUMERIC12          not null,
   constraint PK_PRESUPUESTOS_DETALLES primary key (ID_PRESUPUESTO, ITEM)
);

/*==============================================================*/
/* Table: SERVICIOS_PRODUCTOS                                   */
/*==============================================================*/
create table SERVICIOS_PRODUCTOS (
   ID_SERVICIO_PRODUCTO SERIAL               not null,
   CODIGO               D_VARCHAR15          not null,
   NOMBRE               D_VARCHAR60          not null,
   DESCRIPCION          D_VARCHAR200         not null,
   ID_CLASIFICACION     INT4                 not null,
   ID_UNIDAD            INT4                 not null,
   MARCA                D_VARCHAR60          not null,
   PRECIO_VENTA         D_NUMERIC12          not null,
   HABILITADO           D_CHAR1              not null,
   IVA                  D_CHAR1              not null,
   constraint PK_SERVICIOS_PRODUCTOS primary key (ID_SERVICIO_PRODUCTO)
);

/*==============================================================*/
/* Index: PRODUCTOS_CODIGO                                      */
/*==============================================================*/
create unique index PRODUCTOS_CODIGO on SERVICIOS_PRODUCTOS (
CODIGO
);

/*==============================================================*/
/* Table: TALLERES                                              */
/*==============================================================*/
create table TALLERES (
   ID_TALLER            SERIAL               not null,
   NOMBRE               D_VARCHAR60          not null,
   DIRECCION            D_VARCHAR200         not null,
   LOCALIDAD            D_VARCHAR60          not null,
   TELEFONO_PRINCIPAL   D_VARCHAR20          not null,
   TELEFONO_SECUNDARIO  D_VARCHAR20          null,
   RUC                  D_VARCHAR15          not null,
   SITIO_WEB            D_VARCHAR60          null,
   ESTADO               D_CHAR1              not null,
   constraint PK_TALLERES primary key (ID_TALLER)
);

/*==============================================================*/
/* Index: TALLERES_NOMBRE                                       */
/*==============================================================*/
create unique index TALLERES_NOMBRE on TALLERES (
NOMBRE
);

/*==============================================================*/
/* Table: UNIDADES                                              */
/*==============================================================*/
create table UNIDADES (
   ID_UNIDAD            SERIAL               not null,
   NOMBRE               D_VARCHAR60          not null,
   SIGLA                D_VARCHAR15          not null,
   constraint PK_UNIDADES primary key (ID_UNIDAD)
);

/*==============================================================*/
/* Index: UNIDADES_NOMBRE                                       */
/*==============================================================*/
create unique index UNIDADES_NOMBRE on UNIDADES (
NOMBRE
);

/*==============================================================*/
/* Index: UNIDADES_SIGLA                                        */
/*==============================================================*/
create unique index UNIDADES_SIGLA on UNIDADES (
SIGLA
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS (
   ID_USUARIO           D_VARCHAR20          not null,
   NOMBRE               D_VARCHAR60          not null,
   CLAVE                D_VARCHAR60          not null,
   CONFIRMACION         D_VARCHAR60          not null,
   FECHA_REGISTRO       D_TIMESTAMP          not null,
   ESTADO               D_CHAR1              not null,
   PERFIL               D_CHAR1              not null,
   constraint PK_USUARIOS primary key (ID_USUARIO)
);

/*==============================================================*/
/* Table: VEHICULO                                              */
/*==============================================================*/
create table VEHICULO (
   ID_VEHICULO          SERIAL               not null,
   PLACA                D_NUMERIC12          not null,
   MARCA                D_VARCHAR60          not null,
   MODELO               D_VARCHAR60          not null,
   ANIO                 D_NUMERIC4           null,
   CHASIS               D_VARCHAR20          not null,
   constraint PK_VEHICULO primary key (ID_VEHICULO)
);

/*==============================================================*/
/* Index: VEHICULOS_PLACA                                       */
/*==============================================================*/
create unique index VEHICULOS_PLACA on VEHICULO (
PLACA
);

/*==============================================================*/
/* Table: VENTAS                                                */
/*==============================================================*/
create table VENTAS (
   ID_VENTA             INT4                 not null,
   ID_TALLER            INT4                 not null,
   NUMERO_FACTURA       D_VARCHAR15          not null,
   ID_PRESUPUESTO       INT4                 null,
   ID_CLIENTE           INT4                 not null,
   FECHA                D_TIMESTAMP          not null,
   FACTURADO_POR        INT4                 not null,
   TOTAL                D_NUMERIC12          not null,
   CONDICION            D_CHAR1              not null,
   OBSERVACION          D_VARCHAR200         null,
   ESTADO               D_CHAR1              not null,
   constraint PK_VENTAS primary key (ID_VENTA)
);

/*==============================================================*/
/* Table: VENTAS_DETALLES                                       */
/*==============================================================*/
create table VENTAS_DETALLES (
   ID_VENTA             INT4                 not null,
   ITEM                 D_INT2               not null,
   ID_SERVICIO_PRODUCTO INT4                 not null,
   DESCRIPCION          D_VARCHAR200         null,
   CANTIDAD             D_NUMERIC8_2         not null,
   PRECIO               D_NUMERIC12          not null,
   SUBTOTAL             D_NUMERIC12          not null,
   constraint PK_VENTAS_DETALLES primary key (ID_VENTA, ITEM)
);

alter table AUDITORIAS
   add constraint FK_AUDITORI_REFERENCE_USUARIOS foreign key (ID_USUARIO)
      references USUARIOS (ID_USUARIO)
      on delete restrict on update restrict;

alter table HORARIOS
   add constraint FK_HORARIOS_REFERENCE_TALLERES foreign key (ID_TALLER)
      references TALLERES (ID_TALLER)
      on delete restrict on update restrict;

alter table PRESUPUESTOS
   add constraint FK_PRESUPUE_REFERENCE_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table PRESUPUESTOS
   add constraint FK_PRESUPUE_REFERENCE_VEHICULO foreign key (ID_VEHICULO)
      references VEHICULO (ID_VEHICULO)
      on delete restrict on update restrict;

alter table PRESUPUESTOS
   add constraint FK_PRESUPUE_REFERENCE_TALLERES foreign key (ID_TALLER)
      references TALLERES (ID_TALLER)
      on delete restrict on update restrict;

alter table PRESUPUESTOS
   add constraint FK_PRESUPUE_REFERENCE_EMPLEADO foreign key (PRESUPUESTADO_POR)
      references EMPLEADOS (ID_EMPLEADO)
      on delete restrict on update restrict;

alter table PRESUPUESTOS_DETALLES
   add constraint FK_PRESUPUE_REFERENCE_PRESUPUE foreign key (ID_PRESUPUESTO)
      references PRESUPUESTOS (ID_PRESUPUESTO)
      on delete cascade on update restrict;

alter table PRESUPUESTOS_DETALLES
   add constraint FK_PRESUPUE_REFERENCE_SERVICIO foreign key (ID_SERVICIO_PRODUCTO)
      references SERVICIOS_PRODUCTOS (ID_SERVICIO_PRODUCTO)
      on delete restrict on update restrict;

alter table SERVICIOS_PRODUCTOS
   add constraint FK_SERVICIO_REFERENCE_CLASIFIC foreign key (ID_CLASIFICACION)
      references CLASIFICACIONES (ID_CLASIFICACION)
      on delete restrict on update restrict;

alter table SERVICIOS_PRODUCTOS
   add constraint FK_SERVICIO_REFERENCE_UNIDADES foreign key (ID_UNIDAD)
      references UNIDADES (ID_UNIDAD)
      on delete restrict on update restrict;

alter table VENTAS
   add constraint FK_VENTAS_REFERENCE_TALLERES foreign key (ID_TALLER)
      references TALLERES (ID_TALLER)
      on delete restrict on update restrict;

alter table VENTAS
   add constraint FK_VENTAS_REFERENCE_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table VENTAS
   add constraint FK_VENTAS_REFERENCE_PRESUPUE foreign key (ID_PRESUPUESTO)
      references PRESUPUESTOS (ID_PRESUPUESTO)
      on delete restrict on update restrict;

alter table VENTAS_DETALLES
   add constraint FK_VENTAS_D_REFERENCE_VENTAS foreign key (ID_VENTA)
      references VENTAS (ID_VENTA)
      on delete cascade on update restrict;

