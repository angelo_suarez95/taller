<?php
	$controlError = true;
	
	echo "<script>
			$('#id_usuario_ayuda').html('');
			$('#nombre_ayuda').html('');
			$('#clave_ayuda').html('');
			$('#confirmacion_ayuda').html('');
			$('#estado_ayuda').html('');
			$('#perfil_ayuda').html('');
		</script>";
		
	if($id_usuario == "")
	{
		echo "<script> $('#id_usuario_ayuda').html('Debe ingresar el usuario!!!'); 
		$('#id_usuario').focus(); </script>";
		$controlError = false;
		return;
	}
	
	if($nombre == "")
	{
		echo "<script> $('#nombre_ayuda').html('Debe ingresar el nombre!!!'); 
		$('#nombre').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($clave == "" || strlen($clave) < 6)
	{
		echo "<script> $('#clave_ayuda').html('Debe ingresar la clave de por lo menos 6 ctres.!!!'); 
		$('#clave').focus(); </script>";
		$controlError = false;
		return;
	}
		
	if($confirmacion == "" || strlen($confirmacion) < 6 || $confirmacion != $clave)
	{
		echo "<script> $('#confirmacion_ayuda').html('Debe ingresar la confirmación de por lo menos 6 ctres, igual a la Clave!!!'); 
		$('#confirmacion').focus(); </script>";
		$controlError = false;
		return;
	}
?>