<?php
	include_once("../../lib/funciones.php");
	
	$id_marca 	= $_POST["id_marca"];
	$nombre 		= $_POST["nombre"];
	
	include_once("marca-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Marca.php");	
	$marca = new Marca($id_marca,$nombre);
		
	if($marca->editarMarca()){
		unset($marca);
		echo "<script> location.href='marca-lista.php';</script>";
	}
?>