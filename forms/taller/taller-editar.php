<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Sistema de Taller</title>
		<script src="../../js/jquery.min.js"></script>
		<script src="../../js/codigo.js"></script>
		<script src="../../js/bootstrap/js/bootstrap.min.js"></script>
		<link href="../../js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../css/mi-estilo.css">
		<!-- select2 -->
		<link rel="stylesheet" type="text/css" href="../../js/select2/css/select2.min.css">
		<script src="../../js/select2/js/select2.min.js"></script>
	</head>
	<body>
		<?php
			date_default_timezone_set("America/Asuncion");
			include_once("../../lib/funciones.php");
			fn_sesion();
			fn_sesion_administrador();
			fn_menu();
			
			include_once("../../clases/Taller.php");
			$id = $_GET["id"];
			$taller = new Taller();
			$taller->recuperarTaller($id);
		?>
		<br>
		<div class="container">
			<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('container');">
				Cerrar&nbsp;<span aria-hidden='true'>&times;</span>
			</button>
			<h1 class="h2">Editar Taller</h1>
			<div id="rs-ajax"></div>
			<form class="horizontal-form">
				<div class="form-group">
					<label for="">Taller</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="id_taller" name="id_taller" 
						placeholder="ID de Taller" maxlength="20" value="<?php echo $taller->getIdTaller(); ?>"
						readonly>
						<small id="id_taller_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Nombre</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="nombre" name="nombre"
						placeholder="Nombre" maxlength="50" value="<?php echo $taller->getNombre(); ?>">
						<small id="nombre_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Direccion</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="direccion" name="direccion"
						placeholder="Direccion" maxlength="50" value="<?php echo $taller->getDireccion(); ?>">
						<small id="direccion_ayuda" class="form-text text-muted"></small>
					</div>
				</div>	
				<div class="form-group">
					<label for="">Localidad</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="localidad" name="localidad"
						placeholder="Localidad" maxlength="50" value="<?php echo $taller->getLocalidad(); ?>">
						<small id="localidad_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Telefono Principal</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_principal" name="telefono_principal"
						placeholder="Telefono Principal" maxlength="15" value="<?php echo $taller->getTelefonoPrincipal(); ?>">
						<small id="telefono_principal_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Telefono Secundario</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="telefono_secundario" name="telefono_secundario"
						placeholder="Telefono Secundario" maxlength="15" value="<?php echo $taller->getTelefonoSecundario(); ?>">
						<small id="telefono_secundario_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">RUC</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="ruc" name="ruc"
						placeholder="Ruc" maxlength="10" value="<?php echo $taller->getRuc(); ?>">
						<small id="ruc_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Email</label>
					<div class="col-xs-1">
						<input type="email" class="form-control" id="email" name="email"
						placeholder="Sitio Web" maxlength="50" value="<?php echo $taller->getEmail(); ?>">
						<small id="email_ayuda" class="form-text text-muted"></small>
					</div>
				</div>
				<div class="form-group">
					<label for="">Estado</label>
					<div class="col-xs-1">
						<select id="estado" name="estado" class="form-control">
							<option value="A"<?php echo 'A' === $taller->getEstado() ? "selected='selected'" : ""; ?> >Activo</option>
							<option value="I"<?php echo 'I' === $taller->getEstado() ? "selected='selected'" : ""; ?> >Inactivo</option>
						</select>
						<small id="estado_ayuda" class="form-text text-muted"></small>
					</div>
				</div>			
				<br>
				<button type="button" class="btn btn-primary" onclick="editarTaller();">Editar</button>
				<button type="button" class="btn btn-success" onclick="location.href='taller-lista.php'">Volver</button>
			</form>
		</div>
	</body>
	<script>
		$("#estado").select2();
		$("#nombre").focus();
	</script>
</html>