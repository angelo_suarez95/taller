<?php
	include_once("../../lib/funciones.php");
	
	$id_taller			= $_POST["id_taller"];
	$nombre 			= $_POST["nombre"];
	$direccion 			= $_POST["direccion"];
	$localidad			= $_POST["localidad"];
	$telefono_principal = $_POST["telefono_principal"];
	$telefono_secundario = $_POST["telefono_secundario"];
	$ruc 				= $_POST["ruc"];
	$email		 		= $_POST["email"];
	$estado 			= $_POST["estado"];
	
	include_once("taller-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/taller.php");
	$taller = new Taller($id_taller,$nombre,$direccion,$localidad,
	$telefono_principal,$telefono_secundario,$ruc,$email,$estado);
		
	if($taller->editarTaller()){
		unset($taller);
		echo "<script> location.href='taller-lista.php';</script>";
	}
?>