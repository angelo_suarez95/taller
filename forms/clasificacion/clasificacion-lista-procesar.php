<?php
	include_once("../../lib/funciones.php");
	include_once("../../clases/Clasificacion.php");
	
	$clasificacion = new Clasificacion();
	$rs = $clasificacion->listarClasificacion();
		
	echo '<table id="lista-datos" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Clasificación</th>
				<th>Opciones</th>
            </tr>
        </thead>
		<tbody>';
	if($rs){
		foreach($rs as $fila)
		{
				
			echo "<tr>
					<td>" . $fila["nombre"] . "</td>
					<td>" . $fila["id_clasificacion"] . "</td>
					<td>
						<a class='btn btn-warning btn-sm' href='clasificacion-editar.php?id=" . $fila["id_clasificacion"] . "'
						data-toggle='tooltip' title='Editar'>
						<i class='fa fa-edit'></i></a>
						<a class='btn btn-danger btn-sm' onclick='borrarClasificacion(" . '"' . $fila["id_clasificacion"] . '"' . ");'
						data-toggle='tooltip' title='Borrar'>
						<i class='fa fa-trash'></i></a>					
					</td>
				</tr>";
		}
	}
		
	echo '</tbody>
	</table>';
	
	fn_setear_datatable("lista-datos");
		
	unset($clasificacion);