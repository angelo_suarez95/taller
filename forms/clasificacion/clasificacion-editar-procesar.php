<?php
	include_once("../../lib/funciones.php");
	
	$id_clasificacion 	= $_POST["id_clasificacion"];
	$nombre 		= $_POST["nombre"];
	
	include_once("clasificaciones-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/Clasificacion.php");	
	$clasificacion = new Clasificacion($id_clasificacion,$nombre);
		
	if($clasificacion->editarClasificacion()){
		unset($clasificacion);
		echo "<script> location.href='clasificacion-lista.php';</script>";
	}
?>