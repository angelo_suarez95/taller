<?php
	include_once("../../lib/funciones.php");
	
	$codigo 			= $_POST["codigo"];
	$nombre 			= $_POST["nombre"];
	$descripcion 		= $_POST["descripcion"];
	$id_clasificacion	= $_POST["id_clasificacion"];
	$id_unidad 			= $_POST["id_unidad"];
	$id_marca 			= $_POST["id_marca"];
	$precio_venta 		= $_POST["precio_venta"];
	$habilitado 		= $_POST["habilitado"];
	$iva 				= $_POST["iva"];
	
	include_once("prodserv-validaciones.php");
	if(!$controlError) return;
	
	include_once("../../clases/ProductoServicio.php");
		
	$producto = new ProductoServicio("",$codigo,$nombre,$descripcion,
									$id_clasificacion,$id_unidad,$id_marca,$precio_venta,$habilitado,$iva);
		
	if($producto->grabarProductoServicio()){
		unset($producto);
		echo "<script> location.href='prodserv-lista.php';</script>";
	}
?>