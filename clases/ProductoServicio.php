<?php
	include_once("Conexion.php");
	
	class ProductoServicio{
		private $id_servicio_producto;
		private $codigo;
		private $nombre;
		private $descripcion;
		private $id_clasificacion;
		private $id_unidad;
		private $id_marca;
		private $precio_venta;
		private $habilitado;
		private $iva;
		private $conexion;

		// Consructor
		function __construct($id_servicio_producto="",$codigo="",$nombre="",$descripcion="",
		$id_clasificacion="",$id_unidad="",$id_marca="",$precio_venta="",$habilitado="",$iva=""){
			$this->id_servicio_producto = $id_servicio_producto;
			$this->codigo = $codigo;
			$this->nombre = $nombre;
			$this->descripcion = $descripcion;
			$this->id_clasificacion = $id_clasificacion;
			$this->id_unidad = $id_unidad;
			$this->id_marca = $id_marca;
			$this->precio_venta = $precio_venta;
			$this->habilitado = $habilitado;
			$this->iva = $iva;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdServicioProducto(){
			return $this->id_servicio_producto;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getCodigo(){
			return $this->codigo;
		}
		
		function getDireccion(){
			return $this->direccion;
		}

		function getDescripcion(){
			return $this->descripcion;
		}
		
		function getIdClasificacion(){
			return $this->id_clasificacion;
		}
				
		function getIdUnidad(){
			return $this->id_unidad;
		}
				
		function getIdMarca(){
			return $this->id_marca;
		}
		
		function getPrecioVenta(){
			return $this->precio_venta;
		}
		
		function getHabilitado(){
			return $this->habilitado;
		}
		function getIva(){
			return $this->iva;
		}
		
		// Métodos Setters
		function setIdServicioProducto($id_servicio_producto){
			$this->id_servicio_producto = $id_servicio_producto;
		}
		
		function setCodigo($codigo){
			$this->codigo = $codigo;
		}
		
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function setDescripcion($descripcion){
			$this->descripcion = $descripcion;
		}
		
		function setIdClasificacion($id_clasificacion){
			$this->id_clasificacion = $id_clasificacion;
		}
				
		function setIdUnidad($id_unidad){
			$this->id_unidad = $id_unidad;
		}
				
		function setIdMarca($id_marca){
			$this->id_marca = $id_marca;
		}
		
		function setPrecioVenta($precio_venta){
			$this->precio_venta = $precio_venta;
		}
				
		function setHabilitado($habilitado){
			$this->habilitado = $habilitado;
		}
						
		function setIva($iva){
			$this->iva = $iva;
		}
			
		// Trae el servicio_producto
		function recuperarProductoServicio($id){
			$sql = "select * from servicios_productos where id_servicio_producto = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_servicio_producto = $fila[0]["id_servicio_producto"];
				$this->codigo = $fila[0]["codigo"];
				$this->nombre = $fila[0]["nombre"];
				$this->descripcion = $fila[0]["descripcion"];
				$this->id_clasificacion = $fila[0]["id_clasificacion"];
				$this->id_unidad = $fila[0]["id_unidad"];
				$this->id_marca = $fila[0]["id_marca"];
				$this->precio_venta = $fila[0]["precio_venta"];
				$this->habilitado = $fila[0]["habilitado"];
				$this->iva = $fila[0]["iva"];
			}
		}
		
		// Lista todos los servicios_productos
		function listarProductosServicios(){
			$sql = "select ps.id_servicio_producto,
					 ps.codigo,
					 ps.nombre,
					 u.nombre as nombre_unidad,
					 c.nombre as nombre_clasificacion,
					 m.nombre as nombre_marca,
					 ps.precio_venta,
					 ps.iva,
					 case ps.habilitado when'S' then
						'Sí'
					 when 'N' then
						'No'
					 end as habilitado
			from     servicios_productos as ps, clasificaciones as c, unidades as u, marcas as m
			where	ps.id_clasificacion = c.id_clasificacion
			and     ps.id_unidad = u.id_unidad
			and     ps.id_marca = m.id_marca order by nombre";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarProductoServicio(){
			$sql = "insert into servicios_productos(codigo,nombre,descripcion,id_clasificacion,id_unidad,
			id_marca,precio_venta,habilitado,iva)
			values('$this->codigo',
			'$this->nombre',
			'$this->descripcion',
			'$this->id_clasificacion',
			'$this->id_unidad',
			'$this->id_marca',
			'$this->precio_venta',
			'$this->habilitado',
			'$this->iva')";
			// echo $sql; return;	
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarProductoServicio(){
			$sql = "update servicios_productos
			set nombre = '$this->nombre',
			descripcion = '$this->descripcion',
			id_clasificacion = '$this->id_clasificacion',
			id_unidad = '$this->id_unidad',
			id_marca = '$this->id_marca',
			precio_venta = '$this->precio_venta',
			habilitado = '$this->habilitado',
			iva = '$this->iva'
			where id_servicio_producto = '$this->id_servicio_producto'";
			// echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarProductoServicio($id){
			$sql = "delete from servicios_productos where id_servicio_producto = '$id'";
			// echo $sql; return;
			return $this->conexion->consultarSql($sql,false);
		}
		
	}
?>