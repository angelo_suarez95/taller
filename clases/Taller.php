<?php
	include_once("Conexion.php");
	
	class taller{
		private $id_taller;
		private $nombre;
		private $direccion;
		private $localidad;
		private $telefono_principal;
		private $telefono_secundario;
		private $ruc;
		private $email;
		private $estado;
		private $conexion;

		// Constructor
		function __construct($id_taller="",$nombre="",$direccion="",$localidad="",
		$telefono_principal="",$telefono_secundario="",$ruc="",$email="",$estado=""){
			$this->id_taller = $id_taller;
			$this->nombre = $nombre;
			$this->direccion = $direccion;
			$this->localidad = $localidad;
			$this->telefono_principal = $telefono_principal;
			$this->telefono_secundario = $telefono_secundario;
			$this->ruc = $ruc;
			$this->email = $email;
			$this->estado = $estado;
			$this->conexion = new Conexion();
		}
		
		function __destruct(){
			$this->conexion = null;
		}
		
		// Métodos Getters
		function getIdTaller(){
			return $this->id_taller;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getDireccion(){
			return $this->direccion;
		}
		
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getTelefonoPrincipal(){
			return $this->telefono_principal;
		}
		
				
		function getTelefonoSecundario(){
			return $this->telefono_secundario;
		}
				
		function getRuc(){
			return $this->ruc;
		}
		
		function getEmail(){
			return $this->email;
		}
		
		function getEstado(){
			return $this->estado;
		}
		
		// Métodos Setters
		function setIdTaller($id_taller){
			$this->id_taller = $id_taller;
		}
		
		function setNombre($nombre){
			$this->nombre = $nombre;
		}
		
		function setDireccion($direccion){
			$this->direccion = $direccion;
		}
		
		function setLocalidad($localidad){
			$this->localidad = $localidad;
		}
		
		function setTelefonoPrincipal($telefono_principal){
			$this->telefono_principal = $telefono_principal;
		}
				
		function setTelefonoSecundario($telefono_secundario){
			$this->telefono_secundario = $telefono_secundario;
		}
				
		function setRuc($ruc){
			$this->ruc = $ruc;
		}
		
		function setEmail($email){
			$this->email = $email;
		}
				
		function setEstado($estado){
			$this->estado = $estado;
		}
			
		// Trae el taller
		function recuperarTaller($id){
			$sql = "select * from talleres where id_taller = '" . $id . "'";
			$fila = $this->conexion->consultarSql($sql);
			if($fila){
				$this->id_taller = $fila[0]["id_taller"];
				$this->nombre = $fila[0]["nombre"];
				$this->direccion = $fila[0]["direccion"];
				$this->localidad = $fila[0]["localidad"];
				$this->telefono_principal = $fila[0]["telefono_principal"];
				$this->telefono_secundario = $fila[0]["telefono_secundario"];
				$this->ruc = $fila[0]["ruc"];
				$this->email = $fila[0]["email"];
				$this->estado = $fila[0]["estado"];
			}
		}
		
		// Lista todos los talleres
		function listarTalleres(){
			$sql = "select id_taller, nombre,direccion,localidad, telefono_principal, telefono_secundario,ruc,email,estado
			from talleres order by id_taller";
			
			$rs = $this->conexion->consultarSql($sql);
			return $rs;
		}
		
		// Graba el registro
		function grabarTaller(){
			$sql = "insert into talleres(nombre,direccion,localidad,telefono_principal,telefono_secundario,
			ruc,email,estado)
			values('$this->nombre',
			'$this->direccion',
			'$this->localidad',
			'$this->telefono_principal',
			'$this->telefono_secundario',
			'$this->ruc',
			'$this->email',
			'$this->estado')";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Editar el registro
		function editarTaller(){
			$sql = "update talleres
			set nombre = '$this->nombre',
			direccion = '$this->direccion',
			localidad = '$this->localidad',
			telefono_principal = '$this->telefono_principal',
			telefono_secundario = '$this->telefono_secundario',
			ruc = '$this->ruc',
			email = '$this->email',
			estado = '$this->estado'
			where id_taller = '$this->id_taller'";
			
			return $this->conexion->consultarSql($sql,false);
		}
		
		// Borrar el registro
		function borrarTaller($id){
			$sql = "delete from talleres where id_taller = '" . $id . "'";
			
			return $this->conexion->consultarSql($sql,false);
		}
	}
?>